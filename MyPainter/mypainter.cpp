#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Kresli()
{
	paintWidget.newImage(800, 600);
	red = ui.spinBox->value();
	green = ui.spinBox_2->value();
	blue = ui.spinBox_3->value();
	
	float koeficient = (ui.lineEdit_5->text()).toFloat();
	paintWidget.DDA(red,green,blue,paintWidget.body);
	uhol = ui.lineEdit_3->text().toInt();
	if (ui.comboBox->currentIndex() == 0)
	{
		paintWidget.TH();
		paintWidget.sort();
		paintWidget.ScanLine(red,green,blue);
	}
	
	if (ui.comboBox_3->currentIndex() == 1)
	{
		paintWidget.clearImage();
		paintWidget.otocenie_vlavo(uhol);
		paintWidget.DDA(red, green, blue, paintWidget.body);
		paintWidget.TH();
		paintWidget.sort();
		paintWidget.ScanLine(red, green, blue);

	}
	if (ui.comboBox_3->currentIndex() == 2)
	{
		paintWidget.clearImage();
		paintWidget.otocenie_vpravo(uhol);
		paintWidget.DDA(red, green, blue, paintWidget.body);
		paintWidget.TH();
		paintWidget.sort();
		paintWidget.ScanLine(red, green, blue);
	}
	if (ui.comboBox_3->currentIndex() == 3)
	{
		paintWidget.clearImage();
		paintWidget.skalovanie(koeficient);
		paintWidget.DDA(red, green, blue, paintWidget.body_pom);
		paintWidget.TH();
		paintWidget.sort();
		paintWidget.ScanLine(red, green, blue);
	}
	if (ui.comboBox_3->currentIndex() == 4)
	{
		paintWidget.clearImage();
		bool x = false, y = false;
		if (ui.radioButton->isChecked()== 0)x = true;
		else y = true;
		paintWidget.skosenie(koeficient, x, y);
		paintWidget.DDA(red, green, blue, paintWidget.body);
		paintWidget.TH();
		paintWidget.sort();
		paintWidget.ScanLine(red, green, blue);
	}
	if (ui.comboBox_3->currentIndex() == 5)
	{
		paintWidget.clearImage();
		paintWidget.preklapanie();
		paintWidget.DDA(red, green, blue, paintWidget.body);
		paintWidget.TH();
		paintWidget.sort();
		paintWidget.ScanLine(red, green, blue);
	}
}

void MyPainter::Vykresli_kruznicu()
{
	polomer = ui.lineEdit->text().toInt();
	pocet_bodov = ui.lineEdit_2->text().toInt();
	paintWidget.Bresenhamov(pocet_bodov,polomer,red,green,blue);
}

void MyPainter::Vymaz()
{
	paintWidget.clearImage();
	paintWidget.body.clear();
}

	