#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <qspinbox.h>
#include <qcombobox.h>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();
	int red, green, blue;

public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void Kresli();
	void Vykresli_kruznicu();
	void Vymaz();

private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;

	int uhol;
	int polomer;
	int pocet_bodov;
	
};

#endif // MYPAINTER_H
