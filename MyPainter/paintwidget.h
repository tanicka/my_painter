#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void Bresenhamov(int pocet_bodov, int polomer,int red, int green, int blue);
	void DDA(int red, int green, int blue,QVector<QPoint>b);
	void TH();
	void ScanLine(int red, int green, int blue);
	void sort();
	void posunutie();
	void otocenie_vlavo(int uhol);
	void otocenie_vpravo(int uhol);
	void skalovanie(float koef);
	void skosenie(float koef,bool x,bool y);
	void preklapanie();
	

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }
	QVector<QPoint>body;
	QVector<QPoint>body_pom;

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);
	

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	

	bool modified;
	bool painting;
	int myPenWidth;
	bool rightclick=false;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	QPoint lastPoint2;
	std::vector<double>surX;
	std::vector<double>surY;

	QVector< QVector< double > >tabulkaHran;
	QVector< QVector< double > >TAH;
	QVector<QPoint>posunutie_body;
	

	
};

#endif // PAINTWIDGET_H
