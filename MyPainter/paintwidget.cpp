#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 5;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();

}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
		QPainter painter(&image);
		painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(lastPoint);
		body.push_back(lastPoint);

		update();
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::RightButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}

	if (event->button() == Qt::RightButton && painting) {
		drawLineTo(event->pos());
		painting = true;
		rightclick = true;
	}
	
	if (rightclick == true) {
		int red, green, blue;
		red = 0; green = 0; blue = 0;
		posunutie();
		TH();
		sort();
		ScanLine(red,green,blue);
		update();
	}
	rightclick = false;
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
//	QColor C(200, 255, 255);
//	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
//	painter.drawLine(lastPoint2, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint2, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint2 = endPoint;
	posunutie_body.push_back(lastPoint2);
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::DDA(int red,int green,int blue,QVector<QPoint>b)
{
	QPainter painter(&image);
	QColor C(red, green, blue);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));
	QMessageBox mbox;

	double dx, dy, steps, xx, yy, x, y;
	int x1, x2, y1, y2, x0, y0;
	for (int i = 0; i < b.size(); i++)
	{
		if (i!=b.size()-1)
		{
			x1 = b[i].x();
			y1 = b[i].y();
			x2 = b[i+1].x();
			y2 = b[i+1].y();
		}

		else
		{
			x1 = b[b.size()-1].x();
			y1 = b[b.size()-1].y();
			x2 = b[0].x();
			y2 = b[0].y();
		}

		dx = x2 - x1;
		dy = y2 - y1;

		if (fabs(dx) > fabs(dy))steps = fabs(dx);
		else steps = fabs(dy);

		xx = dx / steps;
		yy = dy / steps;

		x = x1;
		y = y1;

		for (int i = 1; i <= steps; i++)
		{
			painter.drawEllipse((int)x, (int)y, 2, 2);
			x += xx;
			y += yy;
		}
		painter.drawEllipse(x, y, 2, 2);
	}
//	b.clear();
	update();
}

void PaintWidget::Bresenhamov(int pocet_bodov,int polomer, int red, int green, int blue)
{
	int xs, ys;
	xs = image.width();
	ys = image.height();
	QPainter painter(&image);
	
	//Vykreslenie bodov do kruhu
	for (int i = 0; i < pocet_bodov; i++)
	{
		int x, y;
		x = (xs / 2) + polomer*cos(i * 2 * M_PI / pocet_bodov);
		y = (ys / 2) + polomer*sin(i * 2 * M_PI / pocet_bodov);
		surX.push_back(x);
		surY.push_back(y);
	}
	surX.push_back(surX[0]);
	surY.push_back(surY[0]);
	QColor C(red, green, blue);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));

	int x, y, dx, dy, k1, k2, k11, k22, px, py, X, Y;

	for (int i = 0; i < pocet_bodov; i++)
	{
		dx = (surX[i + 1] - surX[i]);
		dy = (surY[i + 1] - surY[i]);

		k1 = 2 * fabs(dy);
		k2 = 2 * fabs(dy) - 2 * fabs(dx);
		k11 = 2 * fabs(dx);
		k22 = 2 * fabs(dx) - 2 * fabs(dy);
		px = 2 * fabs(dy) - fabs(dx);
		py = 2 * fabs(dx) - fabs(dy);

		if (fabs(dy) <= fabs(dx))
		{
			if (dx >= 0) { x = surX[i]; y = surY[i]; X = surX[i + 1]; }
			else { x = surX[i + 1]; y = surY[i + 1]; X = surX[i]; }
			painter.drawEllipse(x, y, 3, 3);

			while (x < X)
			{
				x = x + 1;
				if (px > 0)
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) y = y + 1;
					else y = y - 1;
					px = px + k2;
				}
				else px = px + k1;
				painter.drawEllipse(x, y, 3, 3);
			}
		}

		else
		{
			if (dy >= 0) { x = surX[i]; y = surY[i]; Y = surY[i + 1]; }
			else { x = surX[i + 1]; y = surY[i + 1]; Y = surY[i]; }
			painter.drawEllipse(x, y, 3, 3);
			while (y < Y)
			{
				y = y + 1;

				if (py > 0)
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) x = x + 1;
					else x = x - 1;
					py = py + k22;
				}
				else py = py + k11;
				painter.drawEllipse(x, y, 3, 3);
			}
		}
	}
}

void PaintWidget::posunutie()
{
	body_pom.clear();
	QPainter painter(&image);
	QColor C(100, 200, 50);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));

	QPoint Z, K;
	Z = posunutie_body[0];
	K = posunutie_body[posunutie_body.size() - 1];

	int posunX = K.x() - Z.x();
	int posunY = K.y() - Z.y();
	for (int i = 0; i < body.size(); i++)
	{
		int noveX, noveY;
		noveX = body[i].x() + posunX;
		noveY = body[i].y() + posunY;
		painter.drawPoint(noveX, noveY);
		body_pom.push_back(QPoint(noveX, noveY));
		printf("%d %d\n", body_pom[i].x(), body_pom[i].y());
	}
	clearImage();
	for (int i = 0; i < body_pom.size()-1; i++)
	{
		painter.drawLine(body_pom[i].x(), body_pom[i].y(),body_pom[i+1].x(), body_pom[i + 1].y());
	}
	painter.drawLine(body_pom[(body_pom.size()-1)].x(), body_pom[(body_pom.size() - 1)].y(), body_pom[0].x(), body_pom[0].y());
	body.clear();
	body = body_pom;
	posunutie_body.clear();
}

void PaintWidget::otocenie_vlavo(int uhol)		//vlavo ide
{
	body_pom.clear();
	int Sx,Sy,X,Y;
	Sx = body[0].x();
	Sy = body[0].y();
	
	QPainter painter(&image);
	body_pom.push_back(body[0]);
	for (int i = 1; i < body.size(); i++)
	{
		X=((int)((body[i].x() - Sx)*cos(M_PI/double(uhol)) + (body[i].y() - Sy)*sin(M_PI/double(uhol)) + Sx));
		Y=((int)(-(body[i].x() - Sx)*sin(M_PI/double(uhol)) + (body[i].y() - Sy)*cos(M_PI/double(uhol)) + Sy));
		body_pom.push_back(QPoint(X, Y));
	}
	body.clear();
	body = body_pom;

	update();
}

void PaintWidget::otocenie_vpravo(int uhol)		//vpravo ide
{
	body_pom.clear();
	int Sx, Sy, X, Y;
	Sx = body[0].x();
	Sy = body[0].y();

	QPainter painter(&image);
	body_pom.push_back(body[0]);
	for (int i = 1; i < body.size(); i++)
	{
		X = ((int)((body[i].x() - Sx)*cos(M_PI / uhol) - (body[i].y() - Sy)*sin(M_PI / uhol) + Sx));
		Y = ((int)((body[i].x() - Sx)*sin(M_PI / uhol) + (body[i].y() - Sy)*cos(M_PI / uhol) + Sy));
		body_pom.push_back(QPoint(X, Y));
	}
	body.clear();
	body = body_pom;

	update();

}

void PaintWidget::skalovanie(float koef)
{	
	QPainter painter(&image);
	body_pom.clear();
	int Sx, Sy, X, Y,x0,y0;
	Sx = body[0].x();
	Sy = body[0].y();
	body_pom.push_back(body[0]);

	for (int i = 1; i < body.size(); i++)
	{
		x0 = body[i].x() - Sx;
		y0 = body[i].y() - Sy;

		x0 = (int)(koef*x0);
		y0 = (int)(koef*y0);

		X = x0 + Sx;
		Y = y0 + Sy;
		body_pom.push_back(QPoint(X, Y));
	}
	body.clear();
	body = body_pom;

	update();
}

void PaintWidget::skosenie(float koef,bool x,bool y)
{
	QPainter painter(&image);
	body_pom.clear();
	int Sx, Sy, X, Y, x0, y0;
	Sx = body[0].x();
	Sy = body[0].y();
	body_pom.push_back(body[0]);

	if (x == true)
	{
		for (int i = 1; i < body.size(); i++)
		{
			x0 = body[i].x() - Sx;
			y0 = body[i].y() - Sy;

			x0 = (int)(x0 + (koef*y0));

			X = x0 + Sx;
			Y = y0+Sy;
			body_pom.push_back(QPoint(X, Y));
		}
	}
	else
	{
		for (int i = 1; i < body.size(); i++)
		{
			x0 = body[i].x() - Sx;
			y0 = body[i].y() - Sy;

			y0 = (int)(y0 + (koef*x0));

			X = x0 + Sx;
			Y = y0 + Sx;
			body_pom.push_back(QPoint(X, Y));
		}
	}
	
	body.clear();
	body = body_pom;
	update();
}

void PaintWidget::preklapanie()
{
	QPainter painter(&image);
	QColor C(255, 0, 0);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));
	body_pom.clear();
	
	//NAKRESLENIE OSI SUMERNOSTI
	QPoint p1, p2;
	p1.setX(body[0].x());
	p1.setY(body[0].y());
	p2.setX(body[0].x());
	p2.setY((body[0].y()-1));
	painter.drawLine(p1.x(),(p1.y()+300), p2.x(),(p2.y()-300));

	int smernica_pX,smernica_pY,a,b,c,X, Y;
	float d;
	//ZISTENIE SMEROVEHO VEKTORU
	smernica_pX = p2.x() - p1.x();
	smernica_pY = p2.y() - p1.y();
	//VYPOCITANIE NORMALOVEHO VEKTORU:PREHODENIE SURADNIC SMEROVEHO+VYMENA 1 ZNAMIENKA
	a = -smernica_pY;
	b = smernica_pX;
	c = -a*p2.x() - b*p2.y();

	for (int i = 0; i < body.size(); i++)
	{
		d = (a*body[i].x() + b*body[i].y() + c) / (a*a + b*b);
		X = (int)(body[i].x() - 2 * a*d);
		Y = (int)(body[i].y() - 2 * b*d);
		body_pom.push_back(QPoint(X, Y));
	}
	body.clear();
	body = body_pom;
	update();
}

void PaintWidget::sort()
{
	for (int i = 0; i < tabulkaHran.size(); i++)
	{
		for (int j = 0; j < tabulkaHran.size(); j++)
		{
			if (tabulkaHran[i][0] < tabulkaHran[j][0])	//MOZNO BUDE TREBA ZMENIT ZNAMIENKO 
			{
				QVector<double> tmp = tabulkaHran[i];
				tabulkaHran[i] = tabulkaHran[j];
				tabulkaHran[j] = tmp;
			}
			else if (tabulkaHran[i][0] == tabulkaHran[j][0])
			{
				if (tabulkaHran[i][1] < tabulkaHran[j][1])	//MOZNO BUDE TREBA ZMENIT ZNAMIENKO 
				{
					QVector<double> tmp = tabulkaHran[i];
					tabulkaHran[i] = tabulkaHran[j];
					tabulkaHran[j] = tmp;
				}
			}
			else if ((tabulkaHran[i][0] == tabulkaHran[j][0]) && (tabulkaHran[i][1] == tabulkaHran[j][1]))
			{
				if (tabulkaHran[i][3] < tabulkaHran[j][3])	//MOZNO BUDE TREBA ZMENIT ZNAMIENKO 
				{
					QVector<double> tmp = tabulkaHran[i];
					tabulkaHran[i] = tabulkaHran[j];
					tabulkaHran[j] = tmp;
				}
			}
		}
	}

/*	for (int i = 0; i <tabulkaHran.size(); i++)
	{
		for (int j = 0; j < tabulkaHran[i].size(); j++)
		{
			printf("%lf ", tabulkaHran[i][j]);
			
		}printf("\n");
	}*/
}

void PaintWidget::TH()
{
	tabulkaHran.clear();
	TAH.clear();
	double w,x1, x2, y1, y2, dx, dy;
	for (int i = 0; i < body.size(); i++)
	{
		if (i != (body.size() - 1))
		{
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}

		else
		{
			x1 = body[body.size() - 1].x();
			y1 = body[body.size() - 1].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}
		
		QVector<double>row;
		if ( (y2 - y1)!= 0)
		{
			if (y1 > y2) 
			{
				std::swap(x1, x2);
				std::swap(y1, y2);
			}
			dy = y2 - y1;
			w = (x2 - x1) / dy;
			row.push_back(y1);				
			row.push_back(x1);
			row.push_back(y2-1);
			row.push_back(w);
			tabulkaHran.push_back(row);
		}
	}
}

void PaintWidget::ScanLine(int red, int green, int blue)
{
	QPainter painter(&image);
	QColor C(red, green, blue);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));
	int ya, ymax, ymin;

	//Ya=Ymin
	QVector<double>Ya;
	for (int i = 0; i < 4; i++) Ya.push_back(tabulkaHran[0][i]);

	ymin = Ya[0];
	ymax = tabulkaHran[(body.size()-1)][2];

	
	for(int ya=ymin;ya<=ymax;ya++)
	{
		//VYTVORENIE TAH
		for (int i = 0; i < body.size(); i++)
		{
			QVector<double>row;
			if (ya == tabulkaHran[i][0])
			{
				row.push_back(tabulkaHran[i][0]);
				row.push_back(tabulkaHran[i][1]);
				row.push_back(tabulkaHran[i][2]);
				row.push_back(tabulkaHran[i][3]);
				TAH.push_back(row);
			}
		}
/*		//LEN VYPIS
		for (int i = 0; i <TAH.size(); i++)
		{
			for (int j = 0; j < TAH[i].size(); j++) 
				printf(" aktivne pred %lf ", TAH[i][j]);
			printf("\n");
		}*/
		//USPORIADANIE TAH PODLA X - SURADNICE
			for (int i = 0; i < TAH.size(); i++)
			{
				for (int j = 0; j < TAH.size(); j++)
				{
					if (TAH[i][1] < TAH[j][1])	
					{
						QVector<double> tmp = TAH[i];
						TAH[i] = TAH[j];
						TAH[j] = tmp;
					}
				}
			}

		//VYPOCITA PRIESECNIKY
		QVector<int>priesecniky;
		priesecniky.clear();
		for (int j = 0; j < TAH.size(); j=j+2)
		{
			
			int xa=0, xaa=0;
			xa = TAH[j][3] * ya - TAH[j][3] * TAH[j][0] + TAH[j][1];
			xaa = TAH[j+1][3] * ya - TAH[j+1][3] * TAH[j+1][0] + TAH[j+1][1];
			priesecniky.push_back(xa);
			priesecniky.push_back(xaa);
		}

		qSort(priesecniky.begin(),priesecniky.end());
		for (int i = 0; i < priesecniky.size(); i=i+2) {
			painter.drawLine(priesecniky[i], ya, priesecniky[i + 1], ya);
			QMessageBox mbox;
			mbox.setText(QString::number(priesecniky[i])+" " +QString::number(priesecniky[i+1]));
		//	mbox.exec();
		}

		//VYMAZANIE HRANY
		for (int i = 0; i < TAH.size(); i++)
		{
			if (TAH[i][2] == ya)
			{
				TAH.removeAt(i);
			}
		}
/*		//LEN VYPIS
		for (int i = 0; i <TAH.size(); i++)
		{
			for (int j = 0; j < TAH[i].size(); j++) printf("aktivne po %lf ", TAH[i][j]);
			printf("\n");
		}*/
	}
}

